## Home lab Infrastructure

## Purpose

This the infrastructure for all the home lab apps. The main purpose for all of these is to improve my technical skills in
devops area.

Main features are:
* Common place for public infrastructure - deployments, pipelines, 3rd party services etc
* Gitlab integration
* SSO for all the apps

**Important:** This is NOT the place where all sensitive stuff are (secrets, auth data, tokens etc). For those,
there is another private project.
## Infra Components


#### Nodes Hardware
* Master: Raspberry PI 4 4GB + SSD Silicon power 250GB
* Worker: Raspberry PI 4 4GB + SSD Silicon power 250GB - Offline ATM


#### Nodes Software
* All PIs run Raspbian OS Lite version
* Main node has NFS server(mounted on `/srv/nfs`)
* As K8S implementation, [K3s](https://rancher.com/docs/k3s/latest/en/) is used.
* For now, nodes are configured manually, with SSH via exchanged keys.

This is the public part of the core for whole infra. All the apps are deployed in the Kubernetes.


## Deployments
Private deployments:
* [Gitlab CI / CD](https://docs.gitlab.com/ee/ci/) integration: Gitlab Runners and K8S agent
* [Traefik](https://traefik.io/) - networking stack inside K8S

Public deployments:
* [Authelia](https://www.authelia.com/) - supply the SSO for all the apps
* [Heimdal](https://heimdall.site/) - application dashboard
* [Plex](https://www.plex.tv/) - media server
* [Home Assistant](https://www.home-assistant.io/) - Home automation - Not online, just tested out
* [Nfs Volume Provisioner](https://github.com/kubernetes-sigs/nfs-subdir-external-provisioner)
* [Personal Vue project](https://gitlab.com/silviun-home-lab/home-lab-vue-app)
* More to come :)

## Networking
* Domain bought from [Cloudflare](https://dash.cloudflare.com/)
* The cluster domain is `*.silviun.net`
* TLS is solved by integrating K8S with LetsEncrypt. The issues certificate is wildcard, for `*.silviun.net`
* Traefik is used as a proxy service(it is bundled in initial K3s installation)

### Development
* All the deployments are done via `kubectl` commands or by using `helm`. Check the `infra` directory



