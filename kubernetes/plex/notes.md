### Technical Information

* Plex's volumes are host binded in `/srv/host/plex/{config|data}`
* Port forward 32400 is needed


curl -k -H "Content-Type: application/json" -X PUT --data-binary @tmp.json http://127.0.0.1:8001/api/v1/namespaces/plex/finalize