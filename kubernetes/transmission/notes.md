Info at https://artifacthub.io/packages/helm/k8s-at-home/transmission


1. helm upgrade --install transmission k8s-at-home/transmission --values transmission.values.yml --namespace transmission --create-namespace
2. grant correct permissions to `/srv/host/plex/data` and `/srv/host/transmission/config`  
