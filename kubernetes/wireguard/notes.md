### This is the just public part. Apply first the private part before (it contains all needed secrets)

### Technical Info
* Port forward for `51820` is needed

### Usage
1. use ` wg genkey | tee private.key | wg pubkey > public.key` to generate private public keys
2. config the client/s in server
3. use `qrencode` : `qrencode -t ANSIUTF8 -r client.conf` to generate QR for terminal
4. or `qrencode -t PNG -r client.conf -o client_conf.png` to generate QR png
5. or `sudo wg-quick up/down ./client.conf`
6. Check DNS server IP to match with adguard service IP