1. k create namespace homeassistant
2. k apply -f home-assistant.volume.yml
3. helm upgrade --install home-assistant k8s-at-home/home-assistant -f values.yml -n homeassistant
4. ad in configuration.yml: 

```yaml
http:
  use_x_forwarded_for: true
  trusted_proxies:
    - 192.168.100.0/24
    - 10.42.0.0/16
```

5. install HACS by running this: `wget -O - https://get.hacs.xyz | bash -`